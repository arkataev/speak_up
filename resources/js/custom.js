/**
 *  Reads url from uploaded image and pastes it
 *  to the html-block
 * @param input
 */
function readURL(input) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img_preview').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#img_upload").change(function(){
	readURL(this);
});


CKEDITOR.replace('user_post_text_preview');     // replace preview-textarea with CKeditor
CKEDITOR.replaceAll('user_post_text_edit');     // replace edit-textarea with CKeditor

$('#preview').click(function () {
	$('#user_post_preview').html(CKEDITOR.instances.user_post_text_preview.getData());
});


$('.dropdown-menu a').click(function () {
	var sort_data = $(this).attr('data-sort').split(' ');
	var data = {
		'url': 'board/index',
		'data': {
			'sort': {
				'param': sort_data[0],
				'direction': sort_data[1],
			}
		}
	}

	send(data).done(function (e) {
		$('#publications').detach();
		$('#list').append(e).fadeIn('slow');
	});
})

$('.deactivate_post').click(function () {
	var data = {
		'url': 'post/deactivate',
		'data': {
			'post_id': $(this).parent().parent().parent().attr('id')	
		}
	};

	send(data).done(function () {
		location.reload();
	});
});

$('.activate_post').click(function () {
	var data = {
		'url': 'post/activate',
		'data':{
			'post_id': $(this).parent().parent().parent().attr('id')	
		}
	};

	send(data).done(function () {
		location.reload();
	});
});

$('.edit_post').click(function () {
	var ck_editor = $(this).parent().find('textarea').attr('id');
	var content = CKEDITOR.instances[ck_editor].getData();

	var data = {
		'url': 'post/edit',
		'data':{
			'post_id': $(this).parent().parent().attr('id').split('_')[3],
			'content': content	
		}
	};

	send(data).done(function () {
		location.reload();
	});

});

function send(data) {
	var request = $.ajax(
		{
			url: data.url,
			method: 'POST',
			data: data.data,
		}
	);

	return request;
};

