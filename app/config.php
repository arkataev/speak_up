<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

// Routs
define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']));
define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/speak_up/');
define('IMG', 'resources/img');

// Namespaces
define('_CONTROLLER', '\\Controller\\');
define('_MODEL', '\\Model\\');
define('_CORE', '\\Core\\');

// Database
const DB_CONFIG = array(
	'host'		=> "",
	'db_name'	=> "",
	'user' 		=> "",
	'password' 	=> "",
	'options'	=> ""
);