<!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?= BASE_URL.'/resources/css/bootstrap.min.css';?>">
	<link rel="stylesheet" href="<?= BASE_URL.'/resources/css/custom.css';?>">
	<title>SpeakUp | Оставь свой отзыв</title>
</head>
<body>

	<!--Modal Start-->
	<div class="modal fade modal_login" tabindex="-1" role="dialog" aria-labelledby="modal_login">
		<div class="modal-dialog modal-sm">
			<div class="modal-content user_login">
				<form class="form" action="board/login" method="POST">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="sr-only" for="login_username">Имя</label>
							<input type="text" class="form-control" id="login_username" name="username" placeholder="Имя" required>
						</div>
						<div class="form-group">
							<label class="sr-only" for="login_password">Пароль</label>
							<input type="password" class="form-control" id="login_password" name="password" placeholder="Пароль" required>
						</div>
						<button type="submit" class="btn btn-warning">Войти</button>
					</div>
					<p class="alert alert-info" id="login_hint">Логин: admin@mail.com <br/> Пароль: 123</p>
				</form>
			</div>
		</div>
	</div>
	<!--Modal End-->

	<div class="main_wrapper">
		<div class="container">
			<?php if (isset($_SESSION['errors'])) :?>
				<?php foreach ($_SESSION['errors'] as $error): ?>
					<div class="alert alert-danger" role="alert">
						<p>Возникла ошибка</p>
						<p><?= $error ;?></p>
					</div>
				<?php endforeach;?>
			<?php endif ;?>
			<div class="publication_form">
				<div class="well">
					<form class="form" action="board/add_post" method="POST" enctype="multipart/form-data">
						<div class="form-inline user_data">
							<div class="form-group">
								<label class="sr-only" for="username">Имя</label>
								<input type="text" class="form-control" id="username" name="username" placeholder="Имя" required>
							</div>
							<div class="form-group">
								<label class="sr-only" for="email">Email</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
							</div>
							<button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#user_response_area" aria-expanded="false" aria-controls="user_response_area">Написать отзыв</button>
							<?php if (!isset($_SESSION['user'])) :?>
								<button id="login_btn" type="button" class="btn btn-warning" data-toggle="modal" data-target=".modal_login">Порулить</button>
							<?php else: ?>
								<button id="logout_btn" onclick="location.href='board/logout'" type="button" class="btn btn-info">Выход</button>
							<?php endif;?>
						</div>
						<div class="collapse" id="user_response_area">
							<textarea class="form-control" name="user_post_text_preview" rows="3" id="user_post_text_preview"></textarea>
							<div class="form-group">
								<label for="img_upload">Загрузить изображение</label>
								<input type="file" id="img_upload" name="img">
								<p class="help-block">Максимальные пропорции 320х240 пикселей в формате JPEG, PNG, GIF. Размер <= 2Кб</p>
							</div>
							<button type="submit" class="btn btn-success">Отправить</button>
							<button type="button" id="preview" class="btn btn-info" data-toggle="collapse" data-target="#post_preview_1" aria-expanded="false" aria-controls="post_preview_1">Посмотреть</button>
						</div>
					</form>
				</div>
			</div>
<!--			Preview Start-->
			<div class="post_preview collapse" id="post_preview_1">
				<div class="well user_post">
					<div class="row">
						<div class="col-md-4 col-xs-6">
							<img id="img_preview" src="http://placehold.it/320x240" alt="" class="thumbnail">
						</div>
						<div class="col-md-8 col-xs-6 user_post_content">
							<div class="user_data">
								<p id="user_data_name">Александр</p>
								<p id="user_data_email">arkataev@gmail.com</p>
							</div>
							<div id="user_post_preview"></div>
						</div>
					</div>
				</div>
			</div>
<!--			Preview end -->

<!--			Sort Start-->
			<div class="dropdown sort_posts">
				<button class="btn btn-info dropdown-toggle" type="button" id="sort_posts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Дата
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="sort_posts">
					<li><a class="sort" data-sort="date_added asc" href="#">По убыванию</a></li>
					<li><a class="sort" data-sort="date_added desc" href="#">По возрастанию</a></li>
				</ul>
			</div>
			<div class="dropdown sort_posts">
				<button class="btn btn-info dropdown-toggle" type="button" id="sort_posts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Имя
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="sort_posts">
					<li><a class="sort" data-sort="username asc" href="#">По убыванию</a></li>
					<li><a class="sort" data-sort="username desc" href="#">По возрастанию</a></li>
				</ul>
			</div>
			<div class="dropdown sort_posts">
				<button class="btn btn-info dropdown-toggle" type="button" id="sort_posts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Email
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="sort_posts">
					<li><a class="sort" data-sort="email asc" href="#" >По убыванию</a></li>
					<li><a class="sort" data-sort="email desc" href="#">По возрастанию</a></li>
				</ul>
			</div>
<!--			Sort End-->
			<div id="list">
				<?= $posts;?>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
	<script src="<?= BASE_URL.'/resources/js/bootstrap.min.js';?>"></script>
	<script src="//cdn.ckeditor.com/4.5.9/basic/ckeditor.js"></script>
	<script src="<?= BASE_URL.'/resources/js/custom.js';?>"></script>
</body>
</html>