<ul id="publications">
	<?php foreach ($posts as $post): ?>
	<?php if ($post->approved == 1 || (isset($user) && $user->is_admin)): ?>
		<li>
			<div class="well user_post" id="<?= $post->post_id;?>">
				<div class="row">
					<div class="col-md-4 col-xs-6">
						<em><?= $post->date_added ;?></em>
						<img src="<?= $post->img_url ? BASE_URL.'/'.$post->img_url : BASE_URL . '/' . "resources/img/Freesample.svg";?>" alt="" class="thumbnail">
					</div>
					<?php if (!$post->approved): ?>
						<span class="label label-default moderation_label">Не просмотрено</span>
					<?php elseif ($post->approved < 0): ?>
						<span class="label label-danger moderation_label">Отклонено</span>
					<?php else: ?>
						<span class="label label-success moderation_label">Одобрено</span>
					<?php endif;?>
					<?php if($post->moderated): ?>
						<span class="label label-warning moderation_label">Изменено модератором</span>
					<?php endif; ?>
					<div class="col-md-8 col-xs-6 user_post_content">
						<div class="user_data" data-user_id="<?=$post->user_id;?>">
							<p class="user_data_name"><?= $post->getAuthor()->username;?></p>
							<a href="board/filter/user/<?=$post->getAuthor()->user_email;?>" class="user_data_email"><?= $post->getAuthor()->user_email;?></a>
						</div>
						<p><?= $post->post_text; ?></p>
					</div>
					<?php if(isset($user) && $user->is_admin) :?>
					<div class="admin_buttons">
						<button type="button" class="btn btn-success activate_post">Хорошо</button>
						<button type="button" class="btn btn-danger deactivate_post">Не хорошо</button>
						<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#edit_user_post_<?= $post->post_id;?>" aria-expanded="false" aria-controls="edit_user_post_<?= $post->post_id;?>">Сделать хорошо</button>
					</div>
				</div>
					<?php endif;?>
			</div>
					<?php if(isset($user) && $user->is_admin) :?>
			<div class="collapse" id="edit_user_post_<?= $post->post_id;?>">
				<div class="well">
					<textarea name="user_post_text_edit" class="user_post_text_edit" id="edit_post_<?= $post->post_id;?>"></textarea>
					<button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#edit_user_post_<?= $post->post_id;?>" aria-expanded="false" aria-controls="edit_user_post">Отменить</button>
					<button type="button" class="btn btn-success edit_post" data-toggle="collapse" data-target="#edit_user_post_<?= $post->post_id;?>" aria-expanded="false" aria-controls="edit_user_post_<?= $post->post_id;?>">Сохранить</button>
				</div>
				<?php endif; ?>
			</div>
		</li>
	<?php endif; ?>
<?php endforeach; ?>
</ul>