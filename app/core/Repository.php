<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

namespace Core;


use PDO, PDOException;

class Repository
{

	public static function connect()
	{
		try{
			$db = new PDO("mysql:host=" . DB_CONFIG['host'] . ";dbname=" . DB_CONFIG['db_name'] . "", DB_CONFIG['user'], DB_CONFIG['password']);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $db;
		}catch (PDOException $e){
			exit('Could not connect to Database');
		}
	}

	public static function get($name, $search=false)
	{
		$db = self::connect();		
		$skey = $search ? key($search) : null;
		$query = $search ? "SELECT * FROM {$name} WHERE {$skey}=:{$skey}" : "SELECT * FROM {$name}";
		$stmt = $db->prepare($query);
		$stmt->execute($search ? $search : null);
		$db = null;

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public static function update($name, array $data)
	{
		$request = [];
		foreach ($data as $key => $value){
			$request[] = $key . '=:' . $key;
		}
		$request = implode(', ', $request);
		
		try {
			$db = self::connect();
			$stmt = $db->prepare("UPDATE {$name} SET {$request} WHERE post_id=:post_id");
			$stmt->execute($data);
			$db = null;
			$error = False;
			$message = "{$name} table updated";
		}catch (PDOException $e){
			$error = True;
			$message = $e->getMessage();
		}
		
		return array('error' => $error, 'message' => $message);
	}

	public static function create($table_name, array $data)
	{
		$request = [];
		$columns = [];
		
		foreach ($data as $key => $value){
			$request[] = ':' . $key;
			$columns[] = $key;
		}
		
		$request = implode(', ', $request);
		$columns = implode(', ', $columns);

		try{
			$db = self::connect();
			$stmt = $db->prepare("INSERT INTO {$table_name} ({$columns}) VALUES ({$request})");
			$stmt->execute($data);
			$error = false;
		}catch (PDOException $e){
			$error = true;
			$message = $e->getMessage();
		}

		return $error ? array('error' => $error, 'message' => $message) : $db->lastInsertId();
	}
}