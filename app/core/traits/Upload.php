<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 28.05.2016
 */

namespace Core;

use Exception;

trait Upload
{
	/**
	 * Process image uploading.
	 * Checks for image size and extension. Also performs some security checks
	 * for PHP tags in file contents and strip tags from file name 
	 * 
	 * @param $file     array   data from $_FILE['file']
	 * @param $path     string  'my/image/folder'         
	 * @return          string  'path/to/image/folder' . 'file_name'
	 * @throws Exception
	 */
	public static function image_upload($file, $path)
	{
		// Check if data exists
		if (empty($file['name'])) { return false; }
		// Strip all tags from file name if any
		strip_tags($file['name']);
		// Creates random string with current date appended
		$create_file_name = function () {
			$rand_str = function ($length){
				$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$rstr = '';
				while (strlen($rstr) < $length){
					$rstr .= $chars[rand(0, strlen($chars) -1)];
				}
				return $rstr;
			};

			return $rand_str(10) . '('. date('m-j-y-G-i') . ')';
		};

		$allowedExts = array ('gif', 'jpeg', 'jpg', 'png');
		$extension = explode('.', $file['name'])[1];
		// Checking extensions and file size
		if (   ($file['type']   == 'image/gif')
			|| ($file['type']   == 'image/jpeg')
			|| ($file['type']   == 'image/jpg')
			|| ($file['type']   == 'image/pjpeg')
			|| ($file['type']   == 'image/x-png')
			|| ($file['type']   == 'image/png')
			&& ($file['size'] < 2000)
			&& in_array($extension, $allowedExts))
		{
			// Check uploading errors
			if ($file['error'] > 0) {
				throw new Exception($file['error']);
			}else {
				// Check file contents for php tags
				$file_content = file_get_contents($file['tmp_name']);
				$forbidden = preg_match_all('/<\?php*|(<\?$)/', $file_content);
				if ($forbidden){
					throw new Exception('file error');
				}else {
					// Create new file name from random string and current date
					$name = $create_file_name();
					move_uploaded_file($file['tmp_name'], $path .  '/' .  $name);

					return $path. '/' . $name;
				}
			}
		}else {
			throw new Exception ('Неправильное расширение файла или файл превышает допустимый размер');
		}
	}

}