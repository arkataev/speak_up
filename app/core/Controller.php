<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

namespace Core;


class Controller
{

	/**
	 * Main view action.
	 * Includes html file or returns it as a string 
	 * 
	 * @param string    $path       name of the view file w/o extension ('index')
	 * @param array     $data       data to pass to the view
	 * @param bool      $get_str    if True function returns html as a string 
	 * @return string
	 */
	protected static function view($path, $data = array(), $get_str = false)
	{
		// Extract variables to enviroment
		extract($data, EXTR_OVERWRITE);
		// start buffering
		ob_start();
		$template = 'app/view/' . $path . ".php";
		// if $get_str !true flush html to output
		if (!$get_str) {
			include_once $template;
			ob_end_flush();
		// else return it as a string 
		}else {
			include_once $template;
			$output = ob_get_contents();
			ob_end_clean();
			
			return $output;
		}
	}
	
	protected static function redirect($url)
	{
		$url = BASE_URL.'/'.$url.'.php';
		return header("Location: {$url}");
	}
}