<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 28.05.2016
 */

namespace Core;


abstract class Model
{

	/**
	 * Gets all (including private) attributes of the caller class
	 * 
	 * @return array    Class attributes array 
	 */
	abstract public static function expose();

	/**
	 * Model constructor.
	 * Automatically gets an array of all Class attributes
	 * and maps values from $data to this attributes 
	 * 
	 * @param array $data       associative array with keys equals to class attributes and values to map 
	 */
	public function __construct(array $data)
	{
		$vars = static::expose();                            // get vars of caller-class
		$map = function ($value, $key) use ($data) {         // map $data values to class vars  
			if (array_key_exists($key, $data)) {
				$this->$key = $data[$key];
			}
		};
		array_walk($vars, $map);
	}
	
}