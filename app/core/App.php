<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

namespace Core;

use ReflectionMethod;

class App
{
	/**
	 * @var string
	 */
	protected $controller = 'board';

	/**
	 * @var string
	 */
	protected $method = 'index';

	/**
	 * @var array
	 */
	protected $params = array();


	public function __construct()
	{
		$url = $this->urlParse();
		
		if (file_exists( ROOT.'app/controller/' . ucfirst($url[0]) . '.php')) {
			$this->controller = $url[0];
			unset($url[0]); 
		}
		
		$controller = _CONTROLLER . ucfirst($this->controller);
		$this->controller = new $controller();

		if( isset($url[1])) {
			if (method_exists($this->controller, $url[1])) {
				$this->method = $url[1];
				unset($url[1]);
			}
		}
		
		$this->params = $url ? array_values($url) : array() ;
		$reflection = new ReflectionMethod($this->controller, $this->method); 
		$reflection->invokeArgs($this->controller, $this->params);

	}


	/**
	 * Takes a curren url as a GET parameter and
	 * returns every string between "/" in the url address as a separate array item.
	 * 
	 * @param void
	 * @return array    parsed url array.
	 */
	protected function urlParse()
	{
		if(isset($_GET['url'])) {

			return explode("/", filter_var( rtrim($_GET['url'], "/"), FILTER_SANITIZE_URL ));
		}
	}
}