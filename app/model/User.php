<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

namespace Model;


use Core\Model;
use Core\Repository;

class User extends Model
{
	private static $key_field = 'user_email';
	private $user_id;
	private $username;
	private $user_email;
	private $is_admin = false;
	private $user_password = null;

	public function __set($name, $value)
	{
		$this->$name = $value;
	}
	
	public function __get($name)
	{
		return $this->$name;
	}

	public static function validate($data)
	{
		$username = $data['username'];
		$password = md5($data['password']);
		$user = self::get(['user_email' => $username]);
		
		return $user->user_password == $password ? $user : false;
	}

	static function expose()
	{
		return get_class_vars(__CLASS__);
	}
	
	public static function get(array $data)
	{
		$user = Repository::get('users', $data);

		return $user ? new self($user[0]) : false;
	}

	public static function create($data)
	{
		// Check if User exists
		$key = self::$key_field;
		// Use unique key to search item in database
		$search_key = array_filter($data, function ($item) use ($key) {
			return $item == $key;
		}, ARRAY_FILTER_USE_KEY);
		$user = self::get($search_key);
		
		if ($user) {
			return $user->user_id;
		}else {
			return Repository::create('users', $data);
		}
	}
}