<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

namespace Model;


use Core\Model;


class Post extends Model
{
	private $post_id = null;
	private $user_id = null;
	private $post_text = '';
	private $img_url = '';
	private $approved = 0;
	private $moderated = false;
	private $date_added = '';


	public function __get($name)
	{
		return $this->$name;
	}
	
	public function __set($name, $value)
	{
		$this->$name = $value;
	}

	public function getAuthor()
	{
		return User::get(['user_id'=>$this->user_id]);
	}

	static function expose()
	{
		return get_class_vars(__CLASS__);
	}
}