<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

namespace Controller;


use Core\Controller;
use Core\Repository;

class Post extends Controller
{

	public function edit()
	{
		$post_id = $_POST['post_id'];
		$content = strip_tags($_POST['content']);

		Repository::update('posts',
			[
			'post_id' => $post_id,
			'post_text' => $content ,
			'moderated' => 1
			]
		);
	}

	public function activate()
	{
		$post_id = $_POST['post_id'];
		Repository::update('posts',
			[
			'post_id' => $post_id,
			'approved' => 1,
			]
		);
	}

	public function deactivate()
	{
		$post_id = $_POST['post_id'];
		Repository::update('posts',
			[
				'post_id' => $post_id,
				'approved' => -1,
			]
		);
	}
}