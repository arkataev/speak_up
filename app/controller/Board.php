<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */

namespace Controller;


use Core\Controller;
use Core\Repository;
use Core\Upload;
use Model\Post;
use Model\User;
use Exception;

class Board extends Controller
{
	use Upload;
	
	/**
	 *  Main action
	 *  if request is AJAX echoes out posts html-string
	 *  else loads index view page
	 *
	 * @param void
	 * @return void
	 */
	public function index()
	{
		// Get post sorting params from request
		$sort_param = isset($_POST['sort']) ? $_POST['sort']['param'] : null;
		$direction = isset($_POST['sort']) ? $_POST['sort']['direction'] : null;
		// Get posts sorted array
		$data['posts'] = self::sort($sort_param, $direction);
		// Get user
		$data['user'] = isset($_SESSION['user']) ? unserialize($_SESSION['user']) : null;
		// If AJAX return posts.php view as string to load into main layout
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			exit(self::view('posts', $data, true));
		}else {
		// else display index page
			$data['posts'] = self::view('posts', $data, true);
			self::view('index', $data);
		// Clear session errors data
			unset($_SESSION['errors']);
		}
	}

	/**
	 *  Reads data from $_POST request and creats new Post entry in database
	 *  If post author exists in DB get it's id as a Post entry parametr
	 *  Else creates a new User entry in DB and gets it's id as a Post parametr
	 *  Redirects to index page on succcess.
	 */
	public function add_post()
	{
		$data = array();
		// Get request data
		$username = isset($_POST['username']) ? $_POST['username'] : null;
		$user_email = isset($_POST['email']) ? $_POST['email'] : null;
		$post_text = isset($_POST['user_post_text_preview']) ? strip_tags($_POST['user_post_text_preview']) : null;

		// Check if all fields are filled
		if (!$username || !$user_email || !$post_text) {
			$_SESSION['errors'][] = 'Заполните все обязательные поля';
		}else {
			// Set image
			try {
				$img = self::image_upload($_FILES['img'], IMG);
				$data['img_url'] = $img;
			}catch (Exception $e){
				$_SESSION['errors'][] = $e->getMessage();
			}
			// Set post author
			$data['user_id'] = User::create(['username' => $username, 'user_email' => $user_email]);
			// Set post content
			$data['post_text'] = $post_text;
			// Create post
			Repository::create('posts', $data);
		}

		return self::redirect('index');
	}

	/**
	 * Recieves user data, validates entries and redirects to index page
	 */
	public function login()
	{
		if (isset($_POST['username']) && isset($_POST['password'])) {
			// Validate user Data
			$user = User::validate(['username' => $_POST['username'], 'password' => $_POST['password']]);
			if ($user) {
				// If user logged in create a User object string
				$user_string = serialize($user);
				// Save User string to current Session
				$_SESSION['user'] = $user_string;
			}else {
				$_SESSION['errors'][] = 'Ошибка авторизации';
			}
		}
		
		return self::redirect('index');
	}

	/**
	 *  Clears session data from cookies
	 */
	public function logout()
	{
		// Unset cookies and destroy session on logout
		if (isset($_COOKIE[session_name()])) {
			setcookie(session_name(), "", time() - 3600);
		}
		session_destroy();

		self::redirect('index');
	}

	/**
	 * Sorts posts by provided parameters and returns sported array
	 * of Post objects
	 *
	 * @param string $param         Sort parameter 'date_added' || 'username' || 'email'
	 * @param string $direction     Sort direction 'asc' || 'desc'
	 * @return array $posts
	 */
	protected static function sort($param='date_added', $direction='asc')
	{
		// Get posts array from DB
		$posts_data = Repository::get('posts');
		$posts = array();
		
		// Format posts data and create Post instances
		foreach( $posts_data as $post ) {
			$post['date_added'] = date("F j, Y, g:i a", strtotime($post['date_added']));
			$posts[] = new Post($post);
		}
		// Sort posts instances
		usort($posts, function($item1, $item2) use ($param, $direction){
			if ($param == 'username') {
				if ($direction == 'desc') { return $item1->getAuthor()->username > $item2->getAuthor()->username; }
				if ($direction == 'asc') { return $item1->getAuthor()->username < $item2->getAuthor()->username; }
			}elseif ($param == 'email') {
				if ($direction == 'desc') { return $item1->getAuthor()->user_email > $item2->getAuthor()->user_email; }
				if ($direction == 'asc') { return $item1->getAuthor()->user_email < $item2->getAuthor()->user_email; }
			}else {
				if ($direction == 'desc') { return $item1->$param > $item2->$param; }
				if ($direction == 'asc') { return $item1->$param < $item2->$param; }
			}
		});

		return $posts;
	}
}