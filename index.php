<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.05.2016
 */
require_once 'app/config.php';
require_once 'vendor/autoload.php';

use Core\App;

session_start([
	'hash_function' => 1,
]);

$app = new App();